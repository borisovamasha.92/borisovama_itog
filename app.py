from flask import Flask, render_template, request
import numpy as np
import tensorflow as tf
from tensorflow import keras

app = Flask(__name__)


@app.route('/', methods=["get", "post"])
def predict():
    message = ""

    if request.method == "POST":
        IW = request.form.get("IW")
        IF = request.form.get("IF")
        VW = request.form.get("VW")
        FP = request.form.get("FP")

        data = [[float(IW), float(IF), float(VW), float(FP)]]

        model_loaded = keras.models.load_model("model_mlp")
        pred = model_loaded.predict(data)

        print(pred[0][0])
        print(pred[0][1])
        message = (F"Предсказание Глубины (Depth)= {pred[0][0]}"
                   F"Предсказание Ширины (Width)=  {pred[0][1]}")

    return render_template("index.html", message = message)
